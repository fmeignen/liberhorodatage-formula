==============
liberhorodatage-formula
==============

A saltstack formula handling installation and configuration of `as@lae`_.

.. note::

    See the full `Salt Formulas installation and usage instructions
    <http://docs.saltstack.com/en/latest/topics/development/conventions/formulas.html>`_.

Available states
================

.. contents::
    :local:

``liberhorodatage``
------------

Installs the liberhorodatage software.

.. _`as@lae`: https://adullact.net/projects/liberhorodatage/
