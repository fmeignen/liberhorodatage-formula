{% from "liberhorodatage/map.jinja" import liberhorodatage with context %}

include:
    - apache
    - apache.mod_php5
    - php


liberhorodatage_dist:
  archive:
    - name: /opt/
    - extracted
    - source: {{ liberhorodatage.dist.url }}
    - archive_format: tar
    - source_hash: {{ liberhorodatage.dist.hash }}
    - keep: True
    - if_missing: /opt/toto

/var/www/liberhorodatage:
  file.symlink:
   - target: /opt/liberhorodatage

/opt/liberhorodatage:
  file.directory:
    - user: apache
    - group: apache
    - recurse:
      - user
      - group    

certificates:
 cmd.run:
 - name: cd /etc/ssl && sh /opt/script-AC-horo.sh

/etc/ssl/liberhorodatage.cnf:
  file.managed:
    - source: /opt/liberhorodatage.cnf


/etc/httpd/conf.d/horodatage.conf:
  file.managed:
    - source: salt://liberhorodatage/apache-vhost.conf
    - template: jinja

httpd:
  service.running:
    - enable: True
    - reload: True
    - watch:
      - file: /etc/httpd/conf.d/horodatage.conf

horodatage.local:
  host.present:
    - ip: 127.0.0.1
